# Домашнее задание**
Формирование dashboard на основе собранных данных с Prometheus

#### Цель:
Сформировать dashboard на основе собранных данных с Prometheus

#### Описание/Пошаговая инструкция выполнения домашнего задания:
> !N.B.: задание рекомендуется выполнять после прохождения модуля по Prometheus!
- использовать собственную графану
- создать своего пользователя
- создать свой team
- разрешить доступ к своей дашборде только своей team
- подключить свой прометеус со своим экпортером к графане
- создать свою папку
- создать свой дашбоард на основе метрик со своего экспортера
- настроить threshold на определенное значение метрики
- настроить alert в телеграм канал (указан в ДЗ про алертинг с заббикса или в свой телеграмм канал)
- сделать снэпшот дашборда
- Как результат ДЗ приложить скриншоты созданного дашборда со всеми настройками
- Предоставить json файл дашборда

Критерии оценки:
0 баллов - задание не выполнено
1 балл - задание выполнено успешно

Рекомендуем сдать до: 19.12.2021

---

# Выполнение:

1. устанавливаем прометей

```shell
minikube start --cpus=2 --memory=3gb --disk-size=20gb --vm-driver=hyperkit
k create ns prom-stack
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm upgrade --install prometheus prometheus-community/kube-prometheus-stack -n prom-stack
k apply -f k8s
```
2. открываем доступ к сервисам

```shell
k port-forward svc/prometheus-kube-prometheus-prometheus -n prom-stack 9090
kubectl port-forward deployment/prometheus-grafana -n prom-stack 3000
```

- prometheus - http://127.0.0.1:9090/
- grafana - http://127.0.0.1:3000/

> grafana creds: `admin:prom-operator`

3. импортируем `json` дашборда и настроек `alerting`


<details>

**метрики**:
```
# HELP test_counter My custom parameter
# TYPE test_counter gauge
test_counter{id="1"} 90
test_counter{id="2"} 91
test_counter{id="3"} 79
test_counter{id="4"} 5
test_counter{id="5"} 46
```

**tg**:
```
**Firing**

Value: [ metric='1' labels={__name__=test_counter, container=nginx, endpoint=exporter, id=1, instance=172.17.0.8:9500, job=nginx, namespace=default, pod=nginx-697c799844-769qx, service=nginx} value=95 ], [ metric='3' labels={__name__=test_counter, container=nginx, endpoint=exporter, id=3, instance=172.17.0.8:9500, job=nginx, namespace=default, pod=nginx-697c799844-769qx, service=nginx} value=93 ], [ metric='5' labels={__name__=test_counter, container=nginx, endpoint=exporter, id=5, instance=172.17.0.8:9500, job=nginx, namespace=default, pod=nginx-697c799844-769qx, service=nginx} value=59 ]
Labels:
 - alertname = test_counter
 - project = otus
Annotations:
Source: http://localhost:3000/alerting/SgKtvmY7z/edit
Silence: http://localhost:3000/alerting/silence/new?alertmanager=grafana&matchers=alertname%3Dtest_counter%2Cproject%3Dotus
Dashboard: http://localhost:3000/d/_abSviL7z
Panel: http://localhost:3000/d/_abSviL7z?viewPanel=2
```

</detauls>

# Links

- [Message templating](https://grafana.com/docs/grafana/latest/alerting/unified-alerting/message-templating/)
- [Template data](https://grafana.com/docs/grafana/latest/alerting/unified-alerting/message-templating/template-data/)
- [QUERY EXAMPLES](https://prometheus.io/docs/prometheus/latest/querying/examples/)
- [Create alerts](https://grafana.com/docs/grafana/latest/alerting/old-alerting/create-alerts/)
